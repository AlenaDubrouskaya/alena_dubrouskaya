package com.training.ad.hw_3;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CardTest {

    @Test
    public void tesAddMoney() {
        Card card = new Card("Valaya", 20.5);
        assertEquals(card.addMoney(20.0), 40.5, 0.01);
    }

    @Test
    public void testDecreaseMoney() {
        Card card = new Card("Valaya", 20.5);
        assertEquals(card.decreaseMoney(20.0), 0.5, 0.01);
    }

    @Test
    public void testExchangeMoney() {
        Card card = new Card("Valaya", 20.5);
        assertEquals(card.exchangeMoney(3.2), 6.40, 0.01);
    }
}
package com.training.ad.hw_1;

import java.util.Arrays;
import java.util.Scanner;

public class SelectFiboOrFactorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите целое число тип алгоритма");
        System.out.println("1 - вычисление ряда чисел Фибоначчи");
        System.out.println("2 - вычисление факториала");
        int algorithmld = in.nextInt();
        System.out.println("Введите тип цикла, которы нужно использовать");
        System.out.println("1 - цикл while");
        System.out.println("2 - цикл do while");
        System.out.println("3 - цикл for");
        int loopType = in.nextInt();
        System.out.println("Введите n - параметр, передаваемый в алгоритм");
        int n = in.nextInt();
        function(algorithmld, loopType, n);
    }

    public static void function(int algorithmld, int loopType, int n) {
        if (algorithmld == 1) {
            System.out.println("Вычисление ряда чисел Фибоначчи");
            fibonachi(loopType, n);

        } else if (algorithmld == 2) {
            System.out.println("Вычисление факториала");
            factorial(loopType, n);

        } else {
            System.out.println("Введите другое число: 1 или 2");
        }
    }

    public static void fibonachi(int loopType, int n) {
        if (loopType == 1) {
            System.out.println("Цикл while");
            fiboLoopWhile(n);

        } else if (loopType == 2) {
            System.out.println("Цикл do while");
            fiboLoopDoWhile(n);

        } else if (loopType == 3) {
            System.out.println("Цикл for");
            fiboLoopFor(n);

        } else {
            System.out.println("Введите другое число: 1, 2, 3");
        }
    }

    public static void fiboLoopWhile(int n) {
        int a[] = new int[n]; // Создаём массив int размером n
        if (n == 0) {
            System.out.println(0);
        } else if (n == 1) {
            System.out.print(0);
            System.out.print(" ");
            System.out.print(1);
        } else if (n >= 2) {
            a[0] = 0;
            a[1] = 1;
            int i = 2;
            while (i < n) {
                a[i] = a[i - 2] + a[i - 1];
                i++;
            }
            System.out.println("Выводим элементы ряда чисел Фибоначи");
            System.out.println(Arrays.toString(a));
        }
    }

    public static void fiboLoopDoWhile(int n) {
        int a[] = new int[n]; // Создаём массив int размером n
        if (n == 0) {
            System.out.println(0);
        } else if (n == 1) {
            System.out.print(0);
            System.out.print(" ");
            System.out.print(1);
        } else if (n >= 2) {
            a[0] = 0;
            a[1] = 1;
            int i = 2;
            do {
                a[i] = a[i - 2] + a[i - 1];
                i++;
            } while (i < n);
            System.out.println("Выводим элементы ряда чисел Фибоначи");
            System.out.println(Arrays.toString(a));
        }
    }

    public static void fiboLoopFor(int n) {
        int[] a = new int[n];
        if (n == 0) {
            System.out.println(0);
        } else if (n == 1) {
            System.out.print(0);
            System.out.print(" ");
            System.out.print(1);
        } else if (n >= 2) {
            a[0] = 0;
            a[1] = 1;
            for (int i = 2; i < n; i++) {
                a[i] = a[i - 1] + a[i - 2];
            }
            System.out.println("Выводим элементы ряда чисел Фибоначи");
            System.out.println(Arrays.toString(a));
        }
    }

    public static void factorial(int loopType, int n) {
        if (loopType == 1) {
            System.out.println("Цикл while");
            factoLoopWhile(n);

        } else if (loopType == 2) {
            System.out.println("Цикл do while");
            factoLoopDoWhile(n);

        } else if (loopType == 3) {
            System.out.println("Цикл for");
            factoLoopFor(n);

        } else {
            System.out.println("Введите другое число: 1, 2, 3");
        }
    }

    public static int factoLoopWhile(int n) {
        int a = 1;
        int i = 1;
        while (i <= n) {
            a = a * i;
            i++;
        }
        System.out.println(a);
        return a;
    }

    public static int factoLoopDoWhile(int n) {
        int a = 1;
        int i = 1;
        do {
            a = a * i;
            i++;
        } while (i <= n);
        System.out.println(a);
        return a;
    }

    public static int factoLoopFor(int n) {
        int a = 1;
        for (int i = 1; i <= n; i++) {
            a = a * i;
        }
        System.out.println(a);
        return a;
    }
}
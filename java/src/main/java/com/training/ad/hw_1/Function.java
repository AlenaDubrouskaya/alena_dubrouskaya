package com.training.ad.hw_1;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Function {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);

        double G = 4 * pow(PI, 2) * pow(a, 3) / (pow(p, 2) * (m1 + m2));
        System.out.println(G);
    }
}
package com.training.ad.hw_3;

public class Main {
    public static void main(String[] args) {
        Card card1 = new Card("Alena", 10);
        System.out.println("Имя " + card1.getName());
        System.out.println("Баланс " + card1.getCardBalance());
        System.out.println("Положили деньги на счет " + card1.addMoney(20));
        System.out.println("Сняли деньги со счета " + card1.decreaseMoney(10));
        System.out.println("Проверка денег на счету " + card1.getCardBalance());
        System.out.println("Деньги в долларах " + card1.exchangeMoney(2.56));

        Card card2 = new Card("Ivan");
        System.out.println("Имя " + card2.getName());
        System.out.println("Баланс " + card2.getCardBalance());
        System.out.println("Положили деньги на счет " + card2.addMoney(15));
        System.out.println("Сняли деньги со счета " + card2.decreaseMoney(11.50));
        System.out.println("Проверка денег на счету " + card2.getCardBalance());
        System.out.println("Деньги в евро " + card2.exchangeMoney(3.2));
    }
}

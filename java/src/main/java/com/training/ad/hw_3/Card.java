package com.training.ad.hw_3;

public class Card {
    private String name;
    private double cardBalance;

    public Card(String name, double cardBalance) {
        this.name = name;
        this.cardBalance = cardBalance;
    }

    public Card(String name) {
        this.name = name;
        this.cardBalance = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCardBalance(double cardBalance) {
        this.cardBalance = cardBalance;
    }

    public String getName() {
        return name;
    }

    public double getCardBalance() {
        return cardBalance;
    }

    public double addMoney(double balance) {
        cardBalance = cardBalance + balance;
        return cardBalance;
    }

    public double decreaseMoney(double balance) {
        cardBalance = cardBalance - balance;
        return cardBalance;
    }

    public double exchangeMoney(double rate) {
        cardBalance = cardBalance / rate;
        return cardBalance;
    }
}
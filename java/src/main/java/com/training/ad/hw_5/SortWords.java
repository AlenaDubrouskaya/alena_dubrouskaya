package com.training.ad.hw_5;

import java.util.Arrays;
import java.util.Scanner;

public class SortWords {
    public static void main(String[] args) {
        System.out.println("Введите текст на английском языке");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String[] word = str.replaceAll("\\p{Punct}", "").toLowerCase().split(" ");
        sortWords(word);
        removeDuplicates(word);
    }

    public static void sortWords(String[] words) {
        Arrays.sort(words);
    }

    public static void print(String[] words) {
        char q;
        for (int i = 0; i < words.length - 1; i++) {
            for (char j = 'a'; j <= 'z'; j++) {
                q = words[i].charAt(0);
                if (q == j) {
                    System.out.print(j + ":" + words[i]);
                    while (words[i].charAt(0) == words[i + 1].charAt(0) && i < words.length - 2) {
                        System.out.print(" ");
                        System.out.print(words[i + 1]);
                        System.out.print(" ");
                        ++i;
                    }
                }
            }
            System.out.println(" ");
        }
        return;
    }

    public static void removeDuplicates(String[] words) {
        int compare = 0;
        words[compare] = words[0];
        int count = 0;
        for (int i = 1; i < words.length; i++) {
            if (!(words[compare]).equalsIgnoreCase(words[i])) {
                compare++;
                count++;
                words[compare] = words[i];
            }
        }
        String[] copiedwords = new String[count + 2];
        System.arraycopy(words, 0, copiedwords, 0, count + 2);
        print(copiedwords);
        return;
    }
}
